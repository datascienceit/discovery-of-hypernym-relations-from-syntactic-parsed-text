# README #
* Here I present some of my code from my masters.
* All information about this project in details is in the report which is in hebrew.

The research we implemented is: 
"Learning syntactic patterns for automatic
hypernym discovery", which you can be find here: http://ai.stanford.edu/~rion/papers/hypernym_nips05.pdf

We defined "dependency path" base on  Lin and Pantel:
http://www.eng.utah.edu/~cs6961/papers/lin-kdd01.pdf

we Used Google Syntactic N-Grams database: 
http://storage.googleapis.com/books/syntactic-ngrams/index.html

The building of the classifier was implemented in R.