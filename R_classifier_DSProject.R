library(glmnet)
library(Matrix)
library(doParallel)
cl <- makePSOCKcluster(4)
registerDoParallel(cl)


#timer
#3 minutes upload df size 700MB
#9 minutes all sourse
#init
#setwd("~/Desktop/New_DSProject/DSProject extras")

#read data
df <- read.csv("stan20_dataset.txt") #data
features <-read.csv("stan20_features.txt",header = FALSE) #features
features= factor(t(features)) #turn list into factor
#v=as.data.frame(t(v))
data<-df[,3:length(df)] # remove the w1,w2
#colnames(df) <- features #get the features as names

pie(table(df[length(df)]))#view genral target relationship

#Column bind the data into one variable
#trainingdata <- rbind(traininginput,trainingoutput) no need

trainIndex<-sample(nrow(data), 2/3*nrow(data))
train <- data[trainIndex,]
test <- data[-trainIndex,]

#train.y=as.factor(train$target) # build the real target factor for train

train.x<-train[,1:(length(train)-1)] # build the train data 
test.x<-test[,1:(length(test)-1)] # build the test data 
train.y=as.factor(train[,length(train)])
test.y = as.factor(test[,length(test)])
#test.y = as.factor(test$target) # build the real target factor for test

#X = sparse.model.matrix( ~ . - df$target, df)
#y = df$target

train.x.mtrx <- sparse.model.matrix( ~ ., train.x) #Matrix with all variables in df except the y variable.
test.x.mtrx <- sparse.model.matrix( ~ ., test.x)

#CV regression model
model <- cv.glmnet(train.x.mtrx, train.y, nfolds = 10, family='binomial', parallel=TRUE)#best 
#save(model,file = "glmnet10stan20.rda)
predicted_y = predict(model,type="response",newx=test.x.mtrx)
predicted_y[predicted_y>0.5]  <- 1#should be 0.5
predicted_y[predicted_y<=0.5] <- 0#should be 0.5
confusion_matrix = ftable(test.y, predicted_y)
accuracy <- 100* (sum(diag(confusion_matrix)) / length(predicted_y))
plot(model)

#Precision = TP / (TP+FP) = svmt[4]/(svmt[4]+svmt[2])
#Recall = TP / (TP+FN) = svmt[4]/(svmt[4]+svmt[3])
#Accuracy = (TP + TN) / (TP + TN + FP + FN) = (svmt[4]+svmt[1])/(svmt[1]+svmt[2]+svmt[3]+svmt[4])
#F1 = 2(Precision*Recall)/(Precision+Recall)
TP = confusion_matrix[4]
TN = confusion_matrix[1]
FP = confusion_matrix[2]
FN = confusion_matrix[3]
Precision = TP / (TP+FP)
Recall = TP / (TP+FN)
Accuracy = (TP + TN) / (TP + TN + FP + FN)
F1 = 2*(Precision*Recall)/(Precision+Recall)
Precision
Recall
Accuracy
F1
#[1] is a TP list,
#[2] is a FP list,
#[3] is a TN list,
#[4] is a FN list,

#getting examples:
df.h = df
colnames(df.h) <- features

trainIndex<-sample(nrow(df.h), 2/3*nrow(df.h))
train.dfh <- df.h[trainIndex,]
test.dfh <- df.h[-trainIndex,]
names = names(test.dfh)

#gettings the words couples
len<-length(predicted_y)
listTP=(c())
listFP=(c())
listTN=(c())
listFN=(c())
for(i in len:1){
  #browser()
  if(predicted_y[i]==test.y[i] && test.y[i]==1 && length(listTP)<20){
    listTP=append(listTP,i)
  }
  else if(predicted_y[i]==test.y[i] && test.y[i]==0 && length(listTN)<20){
    listTN=append(listTN,i)
  } 
  else if(predicted_y[i]!=test.y[i] && test.y[i]==0 && length(listFP)<20){
    listFP=append(listFP,i)
  }
  else if(predicted_y[i]!=test.y[i] && test.y[i]==1 && length(listFN)<20){
    listFN=append(listFN,i)
  }
  # browser()
  if(length(listTP)==20 && length(listTN)==20 && length(listFP)==20 && length(listFN)==20){
    break;
  }
}

print("")
print("List TP: ")
for(i in 1:20){
  index = listTP[i]
  print(c(as.character(test.dfh[index,1]),as.character(test.dfh[index,2])))
}
print("")
print("List TN: ")
for(i in 1:20){
  index = listTN[i]
  print(c(as.character(test.dfh[index,1]),as.character(test.dfh[index,2])))
}
print("")
print("List FP: ")
for(i in 1:20){
  index = listFP[i]
  print(c(as.character(test.dfh[index,1]),as.character(test.dfh[index,2])))
}
print("")
print("List FN: ")
for(i in 1:20){
  index = listFN[i]
  print(c(as.character(test.dfh[index,1]),as.character(test.dfh[index,2])))
}

TP.r = as.data.frame(test.dfh[listTP,])
TN.r = as.data.frame(test.dfh[listTN,])
FP.r = as.data.frame(test.dfh[listFP,])
FN.r = as.data.frame(test.dfh[listFN,])

name.length = length(names)
"FP examples:"
for(j in 1:20){
  for(i in 1 : name.length){
    if(FP.r[j,i]==1){
      couple = c(as.character(FP.r[j,1]),as.character(FP.r[j,2]),names[i])
      print(couple)
    }
  }
}

"FN examples:"
for(j in 1:20){
  for(i in 1 : name.length){
    if(FN.r[j,i]==1){
      couple = c(as.character(FN.r[j,1]),as.character(FN.r[j,2]),names[i])
      print(couple)
    }
  }
}

"TP examples:"
for(j in 1:20){
  for(i in 1 : name.length){
    if(TP.r[j,i]==1){
      couple = c(as.character(TP.r[j,1]),as.character(TP.r[j,2]),names[i])
      print(couple)
    }
  }
}

"TN examples:"
for(j in 1:20){
  for(i in 1 : name.length){
    if(TN.r[j,i]==1){
      couple = c(as.character(TN.r[j,1]),as.character(TN.r[j,2]),names[i])
      print(couple)
    }
  }
}
stopCluster(cl)
