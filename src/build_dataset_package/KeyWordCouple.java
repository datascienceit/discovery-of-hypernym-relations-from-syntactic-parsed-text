package build_dataset_package;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class KeyWordCouple implements WritableComparable<KeyWordCouple> {

    protected Text word1;
    protected Text word2;
    protected Text targetValue;

    //Constructor
    public KeyWordCouple() {
        this.targetValue = new Text();
        this.word1 = new Text();
        this.word2 = new Text();
    }

    public KeyWordCouple(Text left, Text right, Text tv) {
        this.targetValue = tv;
        this.word1 = left;
        this.word2 = right;
    }

    public KeyWordCouple(Text left, Text right) {
        this.targetValue = new Text("-1");
        this.word1 = left;
        this.word2 = right;
    }


    //getters
    public Text getTargetValue() {
        return this.targetValue;
    }

    public Text getWord1() {
        return this.word1;
    }

    public Text getWord2() {
        return this.word2;
    }


    //setters
    public void setTargetValue(Text t) {
        this.targetValue = t;
    }

    public void setword1(Text w1) {
        this.word1 = w1;
    }

    public void setword2(Text w2) {
        this.word2 = w2;
    }


    @Override
    public String toString() {
        return "(" + getWord1() + "," + getWord2() + "," + getTargetValue() + ")";
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.targetValue.readFields(in);
        this.word1.readFields(in);
        this.word2.readFields(in);

    }

    public void write(DataOutput out) throws IOException {
        this.targetValue.write(out);
        this.word1.write(out);
        this.word2.write(out);
    }


    /**
     * # #
     * x *
     * x a
     * x c
     * x d
     * . . .
     * y *
     * y a
     * y b
     * <p/>
     * <p/>
     * order : word1 > word2 # > Lexicographic
     * <p/>
     * * @param other
     *
     * @return
     */
    public int compareTo(KeyWordCouple other) {

        String thisWord1 = this.getWord1().toString();
        String thisWord2 = this.getWord2().toString();
        String otherWord1 = other.getWord1().toString();
        String otherWord2 = other.getWord2().toString();
//        if (thisPath.equals("*")) {
//            return 1;
//        }
//        if(otherPath.equals("*")){
//            return -1;
//        }
        if (thisWord1.compareTo(otherWord1) == 0) {
            return thisWord2.compareTo(otherWord2);
        } else
            return thisWord1.compareTo(otherWord1);
    }
}