package build_dataset_package;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Created by yonatanalexander on 7/10/14.
 */
public class MainBuildDataset {

//    private static final int NUM_OF_REDUCERS = 1;

    /**
     * **************** main for local runs *************************
     */

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.setInt("mapred.tasktracker.reduce.tasks.maximum", 1);
        conf.set("hypernymFile", args[2]);
        conf.set("vectorFile", args[3]);
//		conf.set("vectorFile", "C:\\Netta\\University\\workspace\\DSProject\\features.txt");
        conf.set("DPMin", args[4]);
//		Defining job parameters
        Job job = new Job(conf, "BuildDataset");
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setJarByClass(DatasetCompute.class);
        job.setMapperClass(DatasetCompute.DatasetComputeMapJob.class);
        job.setMapOutputKeyClass(KeyWordCouple.class);
        job.setMapOutputValueClass(Text.class);
//		job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
        job.setCombinerClass(DatasetCompute.DatasetComputeCombinerjob.class);
        job.setReducerClass(DatasetCompute.DatasetComputeReduceJob.class);
//		job.setNumReduceTasks(NUM_OF_REDUCERS);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
//		FileInputFormat.addInputPath(job, new Path(args[0]));
        TextInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        boolean result = job.waitForCompletion(true);
        //System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());



		/*Counters counters = job.getCounters();
        System.out.println();
        System.out.println("All Info: Job1");
        for (CounterGroup group : counters) {
            System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
            System.out.println("  number of counters in this group: " + group.size());
            for (Counter counter : group) {
                System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
            }
        }*/

        System.exit(result ? 0 : 1);
    }
}

    /**
     * **************** main for S3 runs *************************
     */

//    public static void main(String[] args) throws Exception {
//        Configuration conf = new Configuration();
//        if (args.length > 2 && args[2].compareToIgnoreCase("inver") == 0){
//			conf.set("inverse", "inver");
//		} else{
//			conf.set("inverse", "stan");
//		}
//		System.out.println("inver is: " + (args[2].compareToIgnoreCase("inver") == 0));
////        conf.setInt("mapred.tasktracker.reduce.tasks.maximum", 1);
//        conf.set("inputPath", args[0]);
////        System.out.println("vectorFile: " + args[2]);
////        conf.set("hypernymFile", args[3]);
////        conf.set("DPMin", args[4]);
//        //Defining job parameters
//        Job job = new Job(conf, "BuildDataset");
////        job.setInputFormatClass(SequenceFileInputFormat.class);
//        job.setInputFormatClass(TextInputFormat.class);
//        job.setJarByClass(DatasetCompute.class);
//        job.setMapperClass(DatasetCompute.DatasetComputeMapJob.class);
//        job.setMapOutputKeyClass(KeyWordCouple.class);
//        job.setMapOutputValueClass(Text.class);
////        job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
//        job.setCombinerClass(DatasetCompute.DatasetComputeCombinerjob.class);
//        job.setReducerClass(DatasetCompute.DatasetComputeReduceJob.class);
////        job.setNumReduceTasks(NUM_OF_REDUCERS);
//        job.setOutputKeyClass(Text.class);
//        job.setOutputValueClass(Text.class);
//
//
////        FileInputFormat.addInputPath(job, new Path(args[0]));
////        TextInputFormat.addInputPath(job, new Path(args[0]));
//        String paths = "";
//        String index;
//
//        for (int i = 0; i < 99; i++) {
//            if (i < 10) {
//                index = "0000" + i;
//            } else {
//                index = "000" + i;
//            }
//
//            try {
//                    FileSystem fs = FileSystem.get(URI.create(args[0]), job.getConfiguration());
//                    fs.open(new Path(args[0]+"biarcs-r-"+index));
////                for local
////                File file = new File(args[0] + "/biarcs-r-" + index);
////                if (!file.exists())
////                    break;
//            } catch (Exception e) {
//            	System.out.println("couldn't open file " +"biarcs-r-"+index+", breaking the loop");
//                break;
//            }
//
//            System.out.println("adding " + "biarcs-r-" + index + " to the input paths");
//            paths += args[0] + "biarcs-r-" + index + ",";
//        }
//
//        System.out.println("the index of the last file that were added: ");
//        // the last character is a "," so we omit it
//        paths = paths.substring(0, paths.length() - 1);
//        FileInputFormat.addInputPaths(job, paths);
//        FileOutputFormat.setOutputPath(job, new Path(args[1]));
//        MultipleOutputs.addNamedOutput(job, "dataset", TextOutputFormat.class, NullWritable.class, Text.class);
//        MultipleOutputs.addNamedOutput(job, "features", TextOutputFormat.class, NullWritable.class, Text.class);
//        boolean result = job.waitForCompletion(true);
//
//
//
//    /*Counters counters = job.getCounters();
//    System.out.println();
//    System.out.println("All Info: Job1");
//    for (CounterGroup group : counters) {
//        System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
//        System.out.println("  number of counters in this group: " + group.size());
//        for (Counter counter : group) {
//            System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
//        }
//    }*/
//
//        System.exit(result ? 0 : 1);
//    }
//}


/*********************************** previous version ************************************/

//package build_dataset_package;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
//import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
//
///**
// * Created by yonatanalexander on 7/10/14.
// */
//public class MainBuildDataset {
//	
//	private static final int NUM_OF_REDUCERS = 1;
//	
//	/******************* main for local runs **************************/
//	
////	public static void main(String[] args) throws Exception{
////		Configuration conf = new Configuration();
////		conf.setInt("mapred.tasktracker.reduce.tasks.maximum", 1);
//////		conf.set("hypernymFile", args[2]);
//////		conf.set("vectorFile", args[3]);
//////		conf.set("vectorFile", "C:\\Netta\\University\\workspace\\DSProject\\features.txt");
//////		conf.set("DPMin", args[4]);
////		//Defining job parameters
////		Job job = new Job(conf, "BuildDataset");
//////		job.setInputFormatClass(SequenceFileInputFormat.class);
////        job.setInputFormatClass(TextInputFormat.class);
////		job.setJarByClass(DatasetCompute.class);
////		job.setMapperClass(DatasetCompute.DatasetComputeMapJob.class);
////		job.setMapOutputKeyClass(KeyWordCouple.class);
////		job.setMapOutputValueClass(Text.class);
//////		job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
////		job.setCombinerClass(DatasetCompute.DatasetComputeCombinerjob.class);
////		job.setReducerClass(DatasetCompute.DatasetComputeReduceJob.class);
////		job.setNumReduceTasks(NUM_OF_REDUCERS);
////		job.setOutputKeyClass(Text.class);
////		job.setOutputValueClass(Text.class);
//////		FileInputFormat.addInputPath(job, new Path(args[0]));
////		TextInputFormat.addInputPath(job, new Path(args[0]));
////		FileOutputFormat.setOutputPath(job, new Path(args[1]));
////		boolean result = job.waitForCompletion(true);
////		//System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());
////
////
////
////		/*Counters counters = job.getCounters();
////        System.out.println();
////        System.out.println("All Info: Job1");
////        for (CounterGroup group : counters) {
////            System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
////            System.out.println("  number of counters in this group: " + group.size());
////            for (Counter counter : group) {
////                System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
////            }
////        }*/
////
////		System.exit(result ? 0 : 1);
////	}
//	
//	/******************* main for S3 runs **************************/
//	
//	public static void main(String[] args) throws Exception{
//        Configuration conf = new Configuration();
//        conf.setInt("mapred.tasktracker.reduce.tasks.maximum", 1);
////        conf.set("vectorFile", args[2]);
////        System.out.println("vectorFile: " + args[2]);
////        conf.set("hypernymFile", args[3]);
////        conf.set("DPMin", args[4]);
//        //Defining job parameters
//        Job job = new Job(conf, "BuildDataset");
////        job.setInputFormatClass(SequenceFileInputFormat.class);
//        job.setInputFormatClass(TextInputFormat.class);
//        job.setJarByClass(DatasetCompute.class);
//        job.setMapperClass(DatasetCompute.DatasetComputeMapJob.class);
//        job.setMapOutputKeyClass(KeyWordCouple.class);
//        job.setMapOutputValueClass(Text.class);
////        job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
//        job.setCombinerClass(DatasetCompute.DatasetComputeCombinerjob.class);
//        job.setReducerClass(DatasetCompute.DatasetComputeReduceJob.class);
//		job.setNumReduceTasks(NUM_OF_REDUCERS);
//        job.setOutputKeyClass(Text.class);
//        job.setOutputValueClass(Text.class);
////        FileInputFormat.addInputPath(job, new Path(args[0]));
//        TextInputFormat.addInputPath(job, new Path(args[0]));
//        FileOutputFormat.setOutputPath(job, new Path(args[1]));
//        boolean result = job.waitForCompletion(true);
//        //System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());
//
//
//
//    /*Counters counters = job.getCounters();
//    System.out.println();
//    System.out.println("All Info: Job1");
//    for (CounterGroup group : counters) {
//        System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
//        System.out.println("  number of counters in this group: " + group.size());
//        for (Counter counter : group) {
//            System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
//        }
//    }*/
//
//        System.exit(result ? 0 : 1);
//    }
//}
