package feature_vector_package;

import org.apache.hadoop.io.Text;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by yonatanalexander on 7/10/14.
 */
public class PathDependencyParser {
    private String dependencyTree;
    static boolean inverse;

    public PathDependencyParser(String dt) {
        dependencyTree = dt;
//        inverse= false;
        //dependencyTree = "a	a/V/ccomp/0 b/NN/nsubj/3 c/NN/advcl/1 d/NN/advcl/1 e/NN/advcl/4	10	1801,2";//TODO remove
    }
    public static boolean isInverse() {
        return inverse;
    }

    public static void setInverse(boolean inverse) {
        PathDependencyParser.inverse = inverse;
    }
    /**
     * @return list of all KeyPathWords crated by the dependency tree
     */
    public ArrayList<KeyPathWords> getKeyPathWords() {
        //get the SNGramList from the dependency tree
        ArrayList<KeyPathWords> ret = null;
        ArrayList<SNGram> SNGramList = parseLineToSNGram();

        if (SNGramList != null) {
            ret = getAllPathsFromSNGramList(SNGramList);//get all paths from the SNGramList
        }
        if(ret != null && ret.size()>0) {
            for (KeyPathWords keyPathWords : ret) {
//                keyPathWords.setSngram(new Text(dependencyTree));
            	String[] depTree = this.dependencyTree.split("\t");
            	keyPathWords.setSngram(new Text(depTree[0]+"\t"+depTree[1]));
            }
        }
//        else
//            System.out.println("SNGramList is null");
//        if (ret != null && ret.size() > 0) {
//            System.out.println("############ found " + ret.size() + " paths ##############");
//            System.out.println(dependencyTree);
//            for (KeyPathWords keyPathWord : ret) {
//                System.out.println("keyPathWord: " + keyPathWord.toString());
//            }
//        }

        return ret;
    }

    private ArrayList<KeyPathWords> getAllPathsFromSNGramList(ArrayList<SNGram> snGramList) {

        ArrayList<KeyPathWords> ret = new ArrayList<KeyPathWords>();
        if (snGramList == null)
            return null;
        SNGram root = null;
        for (SNGram s : snGramList) {
            if (s.getHeadIndex() == 0) {
                root = s;
                break;
            }
        }
        if (root != null) {
            ArrayList<SNGram> rootNeighbours = root.getOffspring();
            if (rootNeighbours.size() < 2)//root has only one direction, no legal dependency path possible
                return null;
            DependencyPath tempPath;
            int i;
            int j;
            //String body = ":" + root.getPos() + "<" + root.getWord() + ">" + root.getPos() + ":";
            String body = "";
            for (i = 0; i < rootNeighbours.size(); i++) {
                for (j = i + 1; j < rootNeighbours.size(); j++) {
                    if (rootNeighbours.get(i).getOriginalIndex() < rootNeighbours.get(j).getOriginalIndex()) {
                        body = createRootBody(root, rootNeighbours.get(i), rootNeighbours.get(j));
                        //System.out.println("getAllPathsFromSNGramList: " +rootNeighbours.get(i).getWord()+ ":" +body + ":" + rootNeighbours.get(j).getWord());
                        tempPath = new DependencyPath(body, 2, rootNeighbours.get(i), rootNeighbours.get(j));
                        tempPath.runNextPath(ret);
                    } else {
                        body = createRootBody(root, rootNeighbours.get(j), rootNeighbours.get(i));
                        //System.out.println("getAllPathsFromSNGramList: " +rootNeighbours.get(j).getWord()+ ":" +body + ":" + rootNeighbours.get(i).getWord());
                        tempPath = new DependencyPath(body, 2, rootNeighbours.get(j), rootNeighbours.get(i));
                        tempPath.runNextPath(ret);
                    }
                }
            }


        }
        return ret;
    }

    private String createRootBody(SNGram root, SNGram left, SNGram right) {
        return left.getPosAndDep() + ":" + root.getPos() + "<" + root.getWord() + ">" + root.getPos() + ":" + right.getDepAndPos();
    }

    private ArrayList<SNGram> parseLineToSNGram() {
        ArrayList<SNGram> SNGramsList = new ArrayList<SNGram>();
        String[] tree = dependencyTree.split("\t");
        //test that the tree has a head and dependency paths
        if (tree.length < 2) {
            return null;
        }
        String[] snGrams = tree[1].split(" ");
        //test for more then two nodes
        if (snGrams.length < 3) {
            return null;
        }
        int i = 1;
        for (String sng : snGrams) {
            if (!validSNGram(sng, snGrams.length)) {
                return null;
            }
            String[] sngSplited = sng.split("/");
            if (sngSplited.length < 4) {
//                System.out.println("one of the SNGrams is illegal");
                return null;
            }
            SNGram tempSNGram = new SNGram(sngSplited[0], sngSplited[1], sngSplited[2], Integer.parseInt(sngSplited[3]));
            tempSNGram.setOriginalIndex(i);
            SNGramsList.add(tempSNGram);
            i++;
        }
        Collections.sort(SNGramsList);
        findParents(SNGramsList);
        return SNGramsList;
    }

    private void printNeighbours(ArrayList<SNGram> snGramsList) {
        for (SNGram snGram : snGramsList) {
            System.out.println(snGram.getWord() + "- Offspring - start");
            printArraylist(snGram.getOffspring());
            System.out.println(snGram.getWord() + "- Offspring  - end");
        }
    }

    private void findParents(ArrayList<SNGram> snGramsList) {
        int i;
        //printArraylist(snGramsList);

        for (SNGram seeker : snGramsList) {
            i = seeker.getHeadIndex();
            if (i == 0) {
                seeker.setParent(null);
                //System.out.println("root is: " + seeker.getWord());
            } else {
                for (SNGram parent : snGramsList) {
                    //System.out.println(seeker.getWord() +" "+ seeker.getHeadIndex() + " " +parent.getWord() + " " + i);
                    if (i == parent.getOriginalIndex()) {
                        seeker.setParent(parent);
                        parent.addOffspring(seeker);
                        //System.out.println("setParent: ("+ seeker.getWord() + " " + parent.getWord() + ") (" + seeker.getHeadIndex()+" " +  i+")");
                        break;
                    }
                }
            }
        }
    }

    private void printArraylist(ArrayList<SNGram> snGramsList) {
        for (int i = 0; i < snGramsList.size(); i++) {
            System.out.println("SNGramsList: " + snGramsList.get(i).getWord() + " " + snGramsList.get(i).getHeadIndex());
        }
    }

    /**
     * a Valid SNGram has:
     * a. 4 attributes spaced with '/'
     * b. first [0],[2] attributes are build only from latters and the [1] can be anything (because of the '$' char)
     * c. the last attribute is an integer, not bigger then the number of nodes
     *
     * @param sng
     * @param lineLength
     * @return
     */
    private boolean validSNGram(String sng, int lineLength) {
        String[] part = sng.split("/");
        if ((part.length != 4) || !part[0].matches("[a-zA-Z]+")
                || !part[2].matches("[a-zA-Z]+") || !part[3].matches("[0-9]+")
                || (part[3].matches("[0-9]+")) && Integer.parseInt(part[3]) > lineLength)
            return false;

        return true;
    }

    private class DependencyPath {

        private String body;
        private int length;
        private SNGram left;
        private SNGram right;

        private DependencyPath(String body) {
            this.body = body;
            stemming();
            this.length = 0;
        }

        private DependencyPath(String body, int length, SNGram left, SNGram right) {
            this.body = body;
            stemming();
            this.length = length;
            this.left = left;
            this.right = right;
        }

        /**
         * valid path is a path that start and end with a noun
         *
         * @return
         */
        public boolean isValidPath() {
            boolean ans = false;
            if (left != null && right != null && left.isNoun() && right.isNoun() && this.length < 5) {
                ans = true;
            }
            return ans;

        }

        public ArrayList<SNGram> getLeftOffspring() {
            return left.getOffspring();
        }

        public ArrayList<SNGram> getRightOffspring() {
            return right.getOffspring();
        }

        /**
         * #######################*
         */
        public void runNextPath(ArrayList<KeyPathWords> ret) {
            if (isValidPath()) {
            	Text left, right, body;
                left = new Text(this.left.getWord());
                right = new Text(this.right.getWord());
                body = new Text(this.getBody());
                ret.add(new KeyPathWords(left, right, body));
            	if (inverse){ //add inverse path to result
            		String inverseBody = inverseBody(this.getBody());
            		if(inverseBody != null) {
                        right = new Text(this.left.getWord());
                        left = new Text(this.right.getWord());
                        body = new Text(inverseBody);
                        ret.add(new KeyPathWords(left, right, body));
                    }
            	}
            }
            if (this.length < 4) {
                ArrayList<SNGram> rightNeighbours = getRightOffspring();
                ArrayList<SNGram> leftNeighbours = getLeftOffspring();
                DependencyPath tempPath = null;
                String pathBody = getBody();
                String tempBody;
                int i;
                for (i = 0; i < rightNeighbours.size(); i++) {//continue right
                    tempBody = pathBody + this.right.getExtentionRight(rightNeighbours.get(i));
                    tempPath = new DependencyPath(tempBody, getLength() + 1, this.getLeft(), rightNeighbours.get(i));
                    tempPath.runNextPath(ret);
                }
                for (i = 0; i < leftNeighbours.size(); i++) {//continue left
                    tempBody = this.left.getExtentionLeft(leftNeighbours.get(i)) + pathBody;
                    tempPath = new DependencyPath(tempBody, getLength() + 1, leftNeighbours.get(i), this.getRight());
                    tempPath.runNextPath(ret);
                }
            }
        }

        private String inverseBody(String body) {
            String[] split = body.split(":");
            String [] subSplit;
            String ans = "";
            boolean firstFlag = true;
            for (int i = split.length-1; i >= 0; i--) {
                if(split[i].contains(">")&&split[i].contains("<")){//this is the root
                    int index = split[i].indexOf(">");
                    String bodyType = split[i].substring(index+1,split[i].length());
                    if(!isVerb(bodyType))
                        return null;
                    String inverseRoot =split[i].substring(0,index)+"@"+split[i].substring(index,split[i].length());
                    ans = ans+":"+inverseRoot;
                }else if(split[i].contains(">")){
                    subSplit = split[i].split(">");
                    if(subSplit.length != 3){
                        System.out.println("subSplit is not length 3: " +split[i]);
                    }
                    ans = ans+":"+subSplit[2]+"<"+subSplit[1]+"<"+subSplit[0];
                }
                else if(split[i].contains("<")){
                    subSplit = split[i].split("<");
                    if(subSplit.length != 3){
                        System.out.println("subSplit is not length 3: " +split[i]);
                    }
                    ans = ans+":"+subSplit[2]+">"+subSplit[1]+">"+subSplit[0];
                }else{
                    if(firstFlag) {
                        ans = ans + split[i];
                        firstFlag = false;
                    }
                    else{
                        ans = ans +":"+ split[i];
                    }

                }
            }
//            System.out.println("original body: " + body );
//            System.out.println("inverse body: " + ans );

            return ans;
        }
        /**
         * NN -	Noun, singular or mass (cat, watermelon...)
         * NNS -	Noun, plural (cats...)
         * NNP	-   Proper noun, singular (London, Microsoft)
         * NNPS -  Proper noun, plural (Everglades)
         *
         * @return
         */
        public boolean isVerb(String s) {
            return s.equals("VB") || s.equals("VBD") ||
                    s.equals("VBG") || s.equals("VBN")||
                    s.equals("VBP")||s.equals("VBZ");
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public SNGram getLeft() {
            return left;
        }

        public void setLeft(SNGram left) {
            this.left = left;
        }

        public SNGram getRight() {
            return right;
        }
        
        public void setRight(SNGram right) {
            this.right = right;
        }
        
        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
            stemming();
        }
        
        /**
         * turning each first and last word of a dependency path to its singular form
         */
        private void stemming(){
        	// stemming
            int len = body.length();
            if (body.toString().substring(0, 3).equalsIgnoreCase("NNS")){
            	body = body.toString().substring(0, 2) + body.toString().substring(3);
            }
            len = body.length();
            if (body.toString().substring(len-3).equalsIgnoreCase("NNS")){
            	body = body.toString().substring(0, len-1);
            }
            len = body.length();
            if (body.toString().substring(0, 4).equalsIgnoreCase("NNPS")){
            	body = body.toString().substring(0, 3) + body.toString().substring(4);
            }
            len = body.length();
            if (body.toString().substring(len-4).equalsIgnoreCase("NNPS")){
            	body = body.toString().substring(0, len-1);
            }
            // stemming - end
        }
    }

    private class SNGram implements Comparable<SNGram> {
        private String word;
        private String pos;
        private String dep;
        private int headIndex;
        private int originalIndex;
        private ArrayList<SNGram> offspring;

        public int getOriginalIndex() {
            return originalIndex;
        }

        public void setOriginalIndex(int originalIndex) {
            this.originalIndex = originalIndex;
        }

        public SNGram getParent() {
            return parent;
        }

        public void setParent(SNGram parent) {
            this.parent = parent;
        }

        private SNGram parent;

        private SNGram(String word, String pos, String dep, int headIndex) {
            this.word = word;
            this.pos = pos;
            this.dep = dep;
            this.headIndex = headIndex;
            this.offspring = new ArrayList<SNGram>();
        }

        public void addOffspring(SNGram s) {
            offspring.add(s);
        }

        public ArrayList<SNGram> getOffspring() {
            return this.offspring;
        }

        private SNGram(String line) {

        }

        @Override
        public String toString() {
            return word + '/' + pos + '/' + dep + '/' + headIndex;
        }

        public String getWord() {
            return word;
        }

        public String getExtentionRight(SNGram right) {
            return ">" + getWord() + ">" + getPos() + ":" + right.getDepAndPos();
        }

        public String getExtentionLeft(SNGram left) {
            return left.getPosAndDep() + ":" + getPos() + "<" + getWord() + "<";
        }

        public void setWord(String word) {
            this.word = word;
        }

        public String getPos() {
            return pos;
        }

        public void setPos(String pos) {
            this.pos = pos;
        }

        public String getDep() {
            return dep;
        }

        public void setDep(String dep) {
            this.dep = dep;
        }

        public int getHeadIndex() {
            return headIndex;
        }

        public void setHeadIndex(int headIndex) {
            this.headIndex = headIndex;
        }

        public boolean hasParents() {
            return (parent != null);
        }

        @Override
        public int compareTo(SNGram o) {//TODO test
            return this.headIndex - o.headIndex;
        }

        /**
         * NN -	Noun, singular or mass (cat, watermelon...)
         * NNS -	Noun, plural (cats...)
         * NNP	-   Proper noun, singular (London, Microsoft)
         * NNPS -  Proper noun, plural (Everglades)
         *
         * @return
         */
        public boolean isNoun() {
            String thisPos = this.getPos();
            return thisPos.equals("NN") || thisPos.equals("NNS") ||
                    thisPos.equals("NNP") || thisPos.equals("NNPS");
        }

        public String getPosAndDep() {
            return getPos() + ":" + getDep();
        }

        /**
         * N:subj:<-buy->:from:N
         * = X buys something from Y
         *
         * @return "pos:word:pos"
         */
        public String getThisAsRoot() {
            return getPos() + "<" + getWord() + ">" + getPos();
        }

        public String getDoublePos() {
            return ":" + getPos() + getPos() + ":";
        }

        public String getDepAndPos() {
            return getDep() + ":" + getPos();
        }
    }
}