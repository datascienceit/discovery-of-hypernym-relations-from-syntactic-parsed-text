package feature_vector_package;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class MainBuildFeatureVector {

        public static void main(String[] args) throws Exception
        {
            Configuration conf = new Configuration();
//    		conf.set("hypernymFile", args[2]);
//    		System.out.println("hypernym file path: " + args[2]);
    		conf.set("DPMin", args[2]);
    		System.out.println("DPMin is: " + args[2]);
    		if (args.length > 3 && args[3].compareToIgnoreCase("inver") == 0){
    			conf.set("inverse", "inver");
    		} else{
    			conf.set("inverse", "stan");
    		}
    		System.out.println("inver is: " + args[3]);

            //Defining job parameters
            Job job = new Job(conf, "CollectFeatures");
            job.setInputFormatClass(SequenceFileInputFormat.class);
            job.setJarByClass(FeatureVectorCompute.class);
            job.setMapperClass(FeatureVectorCompute.FeatureVectorMapJob.class);
            job.setMapOutputKeyClass(KeyPathWords.class);
            job.setMapOutputValueClass(LongWritable.class);
            //job.setPartitionerClass(FeatureVectorCompute.PartitionerJob.class);
            job.setCombinerClass(FeatureVectorCompute.FeatureVectorCombinerJob.class);
            job.setReducerClass(FeatureVectorCompute.FeatureVectorReduceJob.class);
            job.setOutputKeyClass(KeyPathWords.class);
            job.setOutputValueClass(Text.class);
            
            
//            ********* basic input addition *******************
//            parameters:
//            	0- InputPath
//            	1- OutputPath
//            	2- DPMin
//              3- invr
            
//            FileInputFormat.addInputPath(job, new Path(args[0]));
//            FileOutputFormat.setOutputPath(job, new Path(args[1]));
//            ********* basic input addition - end *************
            
            
            
            // ************* multiple input addition ************
            // parameters:
            	// 0 - DPMin
            	// 1 - numOfFiles - the number of files from the biarcs database that
            	//		we want to compute. 20 -> 20% (20 files). 100 -> 100% (100 files)

            String paths = "";
            String index;
            int numOfFiles = Integer.parseInt(args[4]);
            System.out.println("number of input files: " + numOfFiles);
            System.out.println("the input paths:");
            for (int i = 0; i < numOfFiles && i < 99; i++ ){
            	if (i < 10){
            		index = "0" + i;
            	} else {
            		index = "" + i;
            	}
            	System.out.println("adding " + "biarcs."+index+"-of-99 to the input paths");
            	paths += args[0] + "biarcs."+index+"-of-99,";
            }

            paths = paths.substring(0, paths.length()-1);
            //comma separated paths to the list of inputs for the map-reduce job
            FileInputFormat.addInputPaths(job, paths);
            FileOutputFormat.setOutputPath(job, new Path(args[1]));
            // ************* multiple input addition - end *********
            MultipleOutputs.addNamedOutput(job, "biarcs", TextOutputFormat.class, NullWritable.class, Text.class);
            MultipleOutputs.addNamedOutput(job, "features", TextOutputFormat.class, NullWritable.class, Text.class);
            
            System.out.println("running");
            boolean result = job.waitForCompletion(true);
            //System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());



        /*Counters counters = job.getCounters();
        System.out.println();
        System.out.println("All Info: Job1");
        for (CounterGroup group : counters) {
            System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
            System.out.println("  number of counters in this group: " + group.size());
            for (Counter counter : group) {
                System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
            }
        }*/

            System.exit(result ? 0 : 1);
        }
}
