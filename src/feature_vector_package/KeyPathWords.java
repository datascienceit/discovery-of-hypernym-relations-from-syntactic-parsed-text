package feature_vector_package;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class KeyPathWords implements WritableComparable<KeyPathWords> {
    protected Text path;
    protected Text word1;
    protected Text word2;
    protected Text sngram;

    //Constructor
    public KeyPathWords() {
        this.path = new Text();
        this.word1 = new Text();
        this.word2 = new Text();
        this.sngram = new Text();
    }

    public KeyPathWords(Text word1, Text word2, Text path) {
        this.path = path;
        this.word1 = word1;
        this.word2 = word2;
    }
    public KeyPathWords(Text word1, Text word2, Text path,Text sn) {
        this.path = path;
        this.word1 = word1;
        this.word2 = word2;
        this.sngram = sn;
    }

    public Text getSngram() {
        return sngram;
    }

    public void setSngram(Text sngram) {
        this.sngram = sngram;
    }

    //getters
    public Text getPath() {
        return this.path;
    }

    public Text getWord1() {
        return this.word1;
    }

    public Text getWord2() {
        return this.word2;
    }

    public String getId() {
        return getPath() + "/" + getWord1() + "/" + getWord2();
    }


    //setters
    public void setPath(Text p) {
        this.path = p;
    }

    public void setword1(Text right) {
        this.word1 = right;
    }

    public void setword2(Text decade) {
        this.word2 = decade;
    }


    @Override
    public String toString() {
        return getPath() + " (" + getWord1() + "," + getWord2() + ") "+ getSngram();
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.path.readFields(in);
        this.word1.readFields(in);
        this.word2.readFields(in);
        this.sngram.readFields(in);

    }

    public void write(DataOutput out) throws IOException {
        this.path.write(out);
        this.word1.write(out);
        this.word2.write(out);
        this.sngram.write(out);
    }

    /**
     * * @param other
     *
     * @return
     */
    public int compareTo(KeyPathWords other) {

        String thisPath = this.getPath().toString();
        String thisWord1 = this.getWord1().toString();
        String thisWord2 = this.getWord2().toString();
        String otherPath = other.getPath().toString();
        String otherWord1 = other.getWord1().toString();
        String otherWord2 = other.getWord2().toString();
        if (thisPath.equals("*")) {
            return 1;
        }
        if(otherPath.equals("*")){
            return -1;
        }
        if(thisPath.compareTo(otherPath)==0){
            if(thisWord1.compareTo(otherWord1)==0){
                return thisWord2.compareTo(otherWord2);
            }else
                return thisWord1.compareTo(otherWord1);

        }else{
            return thisPath.compareTo(otherPath);
        }
    }
}
