package feature_vector_package;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by yonatanalexander on 7/10/14.
 */
public class FeatureVectorCompute {

    public static class FeatureVectorMapJob extends Mapper<LongWritable, Text, KeyPathWords, LongWritable> {

        private HashMap<String, Boolean> relevantWords;
        private boolean inverse;
//        private boolean onced;


        @Override
        public void setup(Context context) throws IOException, InterruptedException {

            this.inverse = context.getConfiguration().get("inverse").compareToIgnoreCase("inver") == 0;

            this.relevantWords = new HashMap<String, Boolean>();
            BufferedReader br = null;
            String line = "";
            System.out.println("reading hypernym file");

            br = new BufferedReader(new InputStreamReader(FeatureVectorCompute.class.getResourceAsStream("hypernym.txt")));

            while ((line = br.readLine()) != null) {
                String[] lineParts = line.split("\t");
                String hypernym, hyponym;
                hyponym = lineParts[0].trim();
                hypernym = lineParts[1].trim();
                boolean status = lineParts[2].trim().toLowerCase().compareTo("true") == 0 ? true : false;
                this.relevantWords.put(hyponym + "," + hypernym, status);
            }
            br.close();
            System.out.println("finished reading hypernym file");
        }


        /**
         * input from ReduceJob1:
         * key = (w1 w2 decade), value = PMI
         * <p/>
         * (non-Javadoc)
         * example input: experience      that/IN/compl/3 patients/NNS/nsubj/3 experience/VB/ccomp/0      3092
         *
         * @see org.apache.hadoop.mapreduce.Mapper map(KEYIN, VALUEIN, org.apache.hadoop.mapreduce.Mapper.Context)
         */
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            //System.out.println("Map Value: " + value);

            // parse the dependency paths (noun to noun) form the biarc line
            PathDependencyParser parser = new PathDependencyParser(value.toString());
            parser.setInverse(this.inverse);
            // get the parsed paths
            ArrayList<KeyPathWords> keyPathWordsesList = parser.getKeyPathWords();
//            System.out.println("the parsed paths:\n"+keyPathWordsesList);

            // go over each path and send it to the Reducer
            if (keyPathWordsesList != null) {
                for (KeyPathWords keyPath : keyPathWordsesList) {
                    String currKey = keyPath.getWord1() + "," + keyPath.getWord2();
                    String currKey2 = keyPath.getWord2() + "," + keyPath.getWord1();
                    if (this.relevantWords.get(currKey) != null ||
                            this.relevantWords.get(currKey2) != null) {
                        context.write(keyPath, new LongWritable(1));
                    }
                }
            }
        }
    }

    public static class FeatureVectorCombinerJob extends Reducer<KeyPathWords, LongWritable, KeyPathWords, LongWritable> {
        @Override
        public void reduce(KeyPathWords key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
            context.write(key, new LongWritable(1));
        }
    }


    public static class FeatureVectorReduceJob extends Reducer<KeyPathWords, LongWritable, Text, Text> {
        /**
         * gives different name ti the features file and biarcs
         */
        MultipleOutputs<Text, Text> mos;
        /**
         * count the number of appearances for each individual path
         */
        private int pathCounter;
        /**
         * keep all relevant paths
         */
        private ArrayList<String> sngrams;
        /**
         * the current path
         * while the current path remains the same, pathCounter is incremented
         */
        private String currPath;

        /**
         * the minimum number of unique noun pair for each dependency path
         * if pathCounter >= threshold, the path will be written to the vector
         */
        private int threshold;

        /**
         * a string of all biarcs related to a specific path
         */
        private StringBuilder pathBiarcs;

        @Override
        public void setup(Context context) {
            this.pathCounter = 0;
            this.currPath = "";
            String thresh = context.getConfiguration().get("DPMin", "5");
            this.threshold = Integer.parseInt(thresh);
//            this.pathBiarcs = new ArrayList<String>();
//            System.out.println(this.threshold);
            this.pathBiarcs = new StringBuilder("");
            mos = new MultipleOutputs<Text, Text>(context);
            sngrams = new ArrayList<String>();
        }


        @Override
        public void reduce(KeyPathWords key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {


            String newPath = key.getPath().toString();
            String biarc = key.getSngram().toString();

            // we encounter the same path -> increment the counter
            if (newPath.compareTo(this.currPath) == 0) {
                sngrams.add(biarc);
                this.pathCounter++;

//              this is the old version
//        		System.out.println("we encounter the same path -> " + pathBiarcs);
//        		if (this.pathBiarcs.toString().compareTo("") == 0){
//        			this.pathBiarcs.append(biarc);
//        		} else {
//        		this.pathBiarcs.append("!!!" + biarc);
//        		}
//        		System.out.println("we encounter the same path -> " + pathBiarcs);

            } else {
                // new path. if the counter of the currPath >= threshold, add currPath to the vector,
                // set pathCounter to 1 and update currPath with the new path
                if (this.pathCounter >= this.threshold) {
                    //context.write(new Text(this.currPath + "###" + this.pathBiarcs), new Text("")); old write
                    //new write
                    mos.write("features", NullWritable.get(), new Text(this.currPath));
                    for (String s : sngrams) {
                        mos.write("biarcs", NullWritable.get(), new Text(s));
                    }
                    sngrams.clear();//clean all biarcs

                }
                this.pathCounter = 1;
                this.currPath = newPath;
                //this.pathBiarcs = new StringBuilder("");
                sngrams.add(biarc);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            super.cleanup(context);
            mos.close();
        }
    }
}
        
//        private String parseBiarcList() {
//            String ans="";
//            if (this.pathBiarcs==null || this.pathBiarcs.size()==0)
//                ans="";
//            else{
//                for(String s : this.pathBiarcs){
//                    ans = ans + "!!!" + s;
//                }
//                ans = ans.substring(3);
//            }
//            return ans;
//        }
//    }
//}