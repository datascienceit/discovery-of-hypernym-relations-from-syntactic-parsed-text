package test_couple;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.*;

public class TestCoupleVectorCompute {

    public static class TestCoupleVectorComputeMapJob extends Mapper<LongWritable, Text, KeyWordCouple, Text> {

        private HashMap<String, Boolean> relevantWords;

        @Override
        public void setup(Context context) throws IOException, InterruptedException {

        	WordsDependencyParser.inverse = true;
            relevantWords = new HashMap<String, Boolean>();
            this.relevantWords = new HashMap<String, Boolean>();
            BufferedReader br = null;
            System.out.println("Mapper-setup: reading hypernym file");
            String word1 = context.getConfiguration().get("word1");
            String word2 = context.getConfiguration().get("word2");
            this.relevantWords.put(word1+","+word2, true);
        }


        /**
         * experience      that/IN/compl/3 patients/NNS/nsubj/3 experience/VB/ccomp/0      3092
         *
         * @see org.apache.hadoop.mapreduce.Mapper map(KEYIN, VALUEIN, org.apache.hadoop.mapreduce.Mapper.Context)
         */
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            WordsDependencyParser parser = new WordsDependencyParser(value.toString());
            HashMap<KeyWordCouple, String> mp = parser.getKeyPathWords();
            //System.out.println("################### Line starts #####################");
            if (mp != null) {
                Iterator it = mp.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    KeyWordCouple kc = (KeyWordCouple) pairs.getKey();
                    String path = pairs.getValue().toString();
                    String currKey = kc.getWord1() + "," + kc.getWord2();
                    String currKey2 = kc.getWord2() + "," + kc.getWord1();
                    //System.out.println(currKey + " " + path);

                    //if(false){
                    if(path != null && path != "") {
                        if (this.relevantWords.get(currKey) != null) {
                            if (this.relevantWords.get(currKey)){
                                kc.setTargetValue(new Text("1"));
                            }
                            else{
                                kc.setTargetValue(new Text("0"));
                            }
                            context.write(kc, new Text(path));
                        } else if (this.relevantWords.get(currKey2) != null) {
                            if (this.relevantWords.get(currKey2)){
                                kc.setTargetValue(new Text("1"));
                            }
                            else{
                                kc.setTargetValue(new Text("0"));
                            }
                            context.write(kc, new Text(path));
                        }
                    }
                    it.remove(); // avoids a ConcurrentModificationException
                }
            }
        }
    }

    public static class DatasetComputeCombinerjob extends Reducer<KeyWordCouple, Text, KeyWordCouple, Text> {
        @Override
        public void reduce(KeyWordCouple key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            HashSet<String> pathSet = new HashSet<String>();
            for (Text value : values){
                pathSet.add(value.toString());
            }
            for(String value : pathSet){
                context.write(key, new Text(value));
            }
        }
    }

//    public static class DatasetComputePartitioner extends Partitioner<KeyWordCouple, Text>{
//		@Override
//		public int getPartition(KeyWordCouple key, Text value, int numPartitions){
//			return 1;
//		}
//	}

    public static class TestCoupleVectorComputeReduceJob extends Reducer<KeyWordCouple, Text, Text, Text> {
        MultipleOutputs<Text, Text> mos;
        private static TreeMap<String,Integer> relevantPaths;
//        private AWSCredentials credentials;
//        private AmazonS3 s3;
        private boolean onced;



        @Override
        public void setup(Context context) throws IOException, InterruptedException {
            this.onced = true;
            mos = new MultipleOutputs<Text, Text>(context);
//        	credentials = new PropertiesCredentials(
//                    DatasetCompute.class
//                            .getResourceAsStream("AwsCredentials.properties"));
//        	s3 = new AmazonS3Client(credentials);

        	relevantPaths = new TreeMap<String, Integer>();
            BufferedReader br = null;
            String path = "";
            String line = "";

            // for S3 runs
//            FileSystem fs = FileSystem.get(URI.create("s3n://dsp-assignment3-netta/aggregate-vectors/"), context.getConfiguration());
            // TODO what is the output directory on S3?
            // TODO should be know and sent in the job flow
            String inputPath = context.getConfiguration().get("vectorPath");
            FileSystem fs = FileSystem.get(URI.create(inputPath), context.getConfiguration());

            // for local runs
//            String vectorFile = context.getConfiguration().get("file_path");



            for (int j = 0;j<99;j++){
            	try{
            		String fileName = "features-r-00";
            		if (j < 10){
            			fileName = fileName + "00" + j;
            		} else if (j < 100) {
            			fileName = fileName + "0" + j;
            		} else {
            			fileName = fileName + j;
            		}
            		DataInputStream in = fs.open(new Path(inputPath+fileName));
            		br = new BufferedReader(new InputStreamReader(in));
//                    br = new BufferedReader(new FileReader(vectorFile+"/"+fileName)); //for local



            		while ((line = br.readLine()) != null){
            			path = line.toString();
            			if(path != null && path != "") {
            				this.relevantPaths.put(path,0);
            			}
            		}
            		br.close();
            		System.out.println("relevantPaths: " + relevantPaths.size());
            	} catch (Exception e){
            		break;
            	}
            }


            // reordering all of the paths in the TreeMap
            int i = 0;
            Iterator it = relevantPaths.entrySet().iterator();
    		while (it.hasNext()) {
    			Map.Entry pairs = (Map.Entry) it.next();
    			this.relevantPaths.put(pairs.getKey().toString(), i);
    			i++;
    		}
        }

        /**
         * '0','1','0',...,'target_value'
         * @param key
         * @param values
         * @param context
         * @throws java.io.IOException
         * @throws InterruptedException
         */
        @Override
        public void reduce(KeyWordCouple key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        	
        	int size = relevantPaths.size()+1;
            int i;
            int pathIndex;
            String observation = key.getWord1()+","+key.getWord2()+",";
            int [] observationArray = new int[size];
            observationArray[size-1] = Integer.parseInt(key.getTargetValue().toString());
           //for each path enter 1 in the observationArray at the path location
            for(Text path : values){
                if(relevantPaths.containsKey(path.toString())){
                    pathIndex = relevantPaths.get(path.toString());
                    observationArray[pathIndex]=1;
                }
            }

            for(i = 0; i < size-1; i++){
                observation = observation+""+observationArray[i]+",";
            }
            //the target value without ','
            observation = observation+ "" + observationArray[size-1]+"";
//            System.out.println("observation: " + key.toString()+ ": " +  observation);
//            context.write(new Text(observation), new Text(""));//original
            mos.write("dataset", NullWritable.get(), new Text(observation));
        }
        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            super.cleanup(context);
            mos.close();
        }
    }
}



/***************** previous version, reading output of the local aggregator *****************/ 



//package build_dataset_package;
//
//import com.amazonaws.auth.AWSCredentials;
//import com.amazonaws.auth.PropertiesCredentials;
//import com.amazonaws.services.s3.AmazonS3;
//import com.amazonaws.services.s3.AmazonS3Client;
//import com.amazonaws.services.s3.model.GetObjectRequest;
//import com.amazonaws.services.s3.model.S3Object;
//import com.amazonaws.services.s3.model.S3ObjectInputStream;
//import org.apache.hadoop.fs.FileSystem;
//import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.LongWritable;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapreduce.Mapper;
//import org.apache.hadoop.mapreduce.Reducer;
//
//import java.io.BufferedReader;
//import java.io.DataInputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.net.URI;
//import java.util.*;
//
////import com.amazonaws.services.s3.model.GetObjectRequest;
////import com.amazonaws.services.s3.model.S3Object;
////import com.amazonaws.services.s3.model.S3ObjectInputStream;
////import java.io.File;
////import java.io.FileReader;
//
///**
// * Created by yonatanalexander on 7/10/14.
// */
//public class DatasetCompute {
//
//    public static class DatasetComputeMapJob extends Mapper<LongWritable, Text, KeyWordCouple, Text> {
//
//        private HashMap<String, Boolean> relevantWords;
//
//        private boolean onced;
//
//
//        @Override
//        public void setup(Context context) throws IOException, InterruptedException {
//            relevantWords = new HashMap<String, Boolean>();
//            this.relevantWords = new HashMap<String, Boolean>();
//            BufferedReader br = null;
//            String line = "";
//            System.out.println("Mapper-setup: reading hypernym file");
////            String hypernymFile = context.getConfiguration().get("hypernymFile");
////            br = new BufferedReader(new FileReader(hypernymFile));
//            br = new BufferedReader(new InputStreamReader(DatasetCompute.class.getResourceAsStream("hypernym.txt")));
//            while ((line = br.readLine()) != null){
//                String[] lineParts = line.split("\t");
//                String hypernym, hyponym;
//                hyponym = lineParts[0].trim();
//                hypernym = lineParts[1].trim();
//                boolean status = lineParts[2].trim().toLowerCase().compareTo("true") == 0 ? true : false;
//                this.relevantWords.put(hyponym+","+hypernym, status);
//            }
//            br.close();
//        }
//
//
//        /**
//         * experience      that/IN/compl/3 patients/NNS/nsubj/3 experience/VB/ccomp/0      3092
//         *
//         * @see org.apache.hadoop.mapreduce.Mapper map(KEYIN, VALUEIN, org.apache.hadoop.mapreduce.Mapper.Context)
//         */
//        @Override
//        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//            WordsDependencyParser parser = new WordsDependencyParser(value.toString());
//            HashMap<KeyWordCouple, String> mp = parser.getKeyPathWords();
//            //System.out.println("################### Line starts #####################");
//            if (mp != null) {
//                Iterator it = mp.entrySet().iterator();
//                while (it.hasNext()) {
//                    Map.Entry pairs = (Map.Entry) it.next();
//                    KeyWordCouple kc = (KeyWordCouple) pairs.getKey();
//                    String path = pairs.getValue().toString();
//                    String currKey = kc.getWord1() + "," + kc.getWord2();
//                    String currKey2 = kc.getWord2() + "," + kc.getWord1();
//                    //System.out.println(currKey + " " + path);
//
//                    //if(false){
//                    if(path != null && path != "") {
//                        if (this.relevantWords.get(currKey) != null) {
//                            if (this.relevantWords.get(currKey)){
//                                kc.setTargetValue(new Text("1"));
//                            }
//                            else{
//                                kc.setTargetValue(new Text("0"));
//                            }
//                            context.write(kc, new Text(path));
//                        } else if (this.relevantWords.get(currKey2) != null) {
//                            if (this.relevantWords.get(currKey2)){
//                                kc.setTargetValue(new Text("1"));
//                            }
//                            else{
//                                kc.setTargetValue(new Text("0"));
//                            }
//                            context.write(kc, new Text(path));
//                        }
//                    }
//                    it.remove(); // avoids a ConcurrentModificationException
//                }
//            }
//        }
//    }
//
//    public static class DatasetComputeCombinerjob extends Reducer<KeyWordCouple, Text, KeyWordCouple, Text> {
//        @Override
//        public void reduce(KeyWordCouple key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
//            HashSet<String> pathSet = new HashSet<String>();
//            for (Text value : values){
//                pathSet.add(value.toString());
//            }
//            for(String value : pathSet){
//                context.write(key, new Text(value));
//            }
//        }
//    }
//
////    public static class DatasetComputePartitioner extends Partitioner<KeyWordCouple, Text>{
////		@Override
////		public int getPartition(KeyWordCouple key, Text value, int numPartitions){
////			return 1;
////		}
////	}
//
//    public static class DatasetComputeReduceJob extends Reducer<KeyWordCouple, Text, Text, Text> {
//        private static TreeMap<String,Integer> relevantPaths;
//        private AWSCredentials credentials;
//        private AmazonS3 s3;
//        private boolean onced;
//        
//        @Override
//        public void setup(Context context) throws IOException {
//            this.onced = true;
//        	credentials = new PropertiesCredentials(
//                    DatasetCompute.class
//                            .getResourceAsStream("AwsCredentials.properties"));
//        	s3 = new AmazonS3Client(credentials);
//        	
//        	relevantPaths = new TreeMap<String, Integer>();
//            BufferedReader br = null;
//            String path = "";
//            String line = "";
//            
//            // for S3 runs
//            FileSystem fs = FileSystem.get(URI.create("s3n://dsp-assignment3-netta/aggregate-vectors/"), context.getConfiguration());
//            DataInputStream in = fs.open(new Path("s3n://dsp-assignment3-netta/aggregate-vectors/features-dpmin5-stan-nof20"));
//            br = new BufferedReader(new InputStreamReader(in));
//            
//            // for local runs
////            String vectorFile = context.getConfiguration().get("vectorFile");
////            br = new BufferedReader(new FileReader(vectorFile));
//
////            S3Object s3obj = s3.getObject(new GetObjectRequest("dsp-assignment3-netta/aggregate-vectors", "features-dpmin5-stan-nof20"));
////            S3ObjectInputStream content = s3obj.getObjectContent();
////            br = new BufferedReader(new InputStreamReader(content));
//            
//
//            int i = 0;
//
//            while ((line = br.readLine()) != null){
//                path = line.toString();
//                if(path != null && path != "") {
//                    this.relevantPaths.put(path,0);
//                }
//            }
//            br.close();
//            System.out.println("relevantPaths: " + relevantPaths.size());
//
//            Iterator it = relevantPaths.entrySet().iterator();
//            while (it.hasNext()) {
//                Map.Entry pairs = (Map.Entry) it.next();
//                this.relevantPaths.put(pairs.getKey().toString(), i);
//                i++;
//            }
////            it = relevantPaths.entrySet().iterator();
////            while (it.hasNext()) {
////                Map.Entry pairs = (Map.Entry) it.next();
////                System.out.println(pairs.getKey() + " " + pairs.getValue());
////            }
//        }
//
//        /**
//         * '0','1','0',...,'target_value'
//         * @param key
//         * @param values
//         * @param context
//         * @throws IOException
//         * @throws InterruptedException
//         */
//        @Override
//        public void reduce(KeyWordCouple key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
//        	String header = "";
//        	if (this.onced){
//        		//write header
//        		for (String str : relevantPaths.keySet()) {
//        			header = header + str +",";
//        		}
//        		header = "w1,w2,"+header + "target";
//        		context.write(new Text(header), new Text(""));
//        		this.onced = false;
//        	}
//        	
//        	int size = relevantPaths.size()+1;
//            int i;
//            int pathIndex;
//            String observation = key.getWord1()+","+key.getWord2()+",";
//            int [] observationArray = new int[size];
//            observationArray[size-1] = Integer.parseInt(key.getTargetValue().toString());
//           //for each path enter 1 in the observationArray at the path location
//            for(Text path : values){
//                if(relevantPaths.containsKey(path.toString())){
//                    pathIndex = relevantPaths.get(path.toString());
//                    observationArray[pathIndex]=1;
//                }
//            }
//
//            for(i = 0; i < size-1; i++){
//                observation = observation+""+observationArray[i]+",";
//            }
//            //the target value without ','
//            observation = observation+ "" + observationArray[size-1]+"";
////            System.out.println("observation: " + key.toString()+ ": " +  observation);
//            context.write(new Text(observation), new Text(""));
//        }
//    }
//}