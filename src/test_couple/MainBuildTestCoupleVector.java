package test_couple;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Created by yonatanalexander on 7/10/14.
 */
public class MainBuildTestCoupleVector {

    private static final int NUM_OF_REDUCERS = 1;

    /**
     * **************** main for local runs *************************
     */

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        if (args.length == 5) {
            conf.set("inputPath", args[0]);
            conf.set("vectorPath", args[2]);
            conf.set("word1", args[3]);
            conf.set("word2", args[4]);

        } else {
            return;
        }
        System.out.println("biuld vector for: (" + args[1] + ", " + args[2] + ")");
        Job job = new Job(conf, "BuildDataset");
//		job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setJarByClass(TestCoupleVectorCompute.class);
        job.setMapperClass(TestCoupleVectorCompute.TestCoupleVectorComputeMapJob.class);
        job.setMapOutputKeyClass(KeyWordCouple.class);
        job.setMapOutputValueClass(Text.class);
//		job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
        job.setCombinerClass(TestCoupleVectorCompute.DatasetComputeCombinerjob.class);
        job.setReducerClass(TestCoupleVectorCompute.TestCoupleVectorComputeReduceJob.class);
        job.setNumReduceTasks(NUM_OF_REDUCERS);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
//		FileInputFormat.addInputPath(job, new Path(args[0]));
        TextInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        boolean result = job.waitForCompletion(true);
        //System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());

        System.exit(result ? 0 : 1);
    }
}
//
////    /**
////     * **************** main for S3 runs *************************
////     */
////
////    public static void main(String[] args) throws Exception {
////        Configuration conf = new Configuration();
////
////        if (args.length == 4){
////			conf.set("word1", args[2]);
////			conf.set("word2", args[3]);
////		} else{
////			return;
////		}
////		System.out.println("biuld vector for: (" + args[1] + ", " + args[2]+")");
////        conf.set("inputPath", args[0]);
////        Job job = new Job(conf, "BuildDataset");
////        job.setInputFormatClass(TextInputFormat.class);
////        job.setJarByClass(TestCoupleVectorCompute.class);
////        job.setMapperClass(TestCoupleVectorCompute.DatasetComputeMapJob.class);
////        job.setMapOutputKeyClass(KeyWordCouple.class);
////        job.setMapOutputValueClass(Text.class);
////        job.setCombinerClass(TestCoupleVectorCompute.DatasetComputeCombinerjob.class);
////        job.setReducerClass(TestCoupleVectorCompute.DatasetComputeReduceJob.class);
////        job.setOutputKeyClass(Text.class);
////        job.setOutputValueClass(Text.class);
////
//////        ********* basic input addition *******************
//////            parameters:
//////            	0- InputPath
//////            	1- OutputPath
//////            	2- word1
//////              3- word2
////
//////            FileInputFormat.addInputPath(job, new Path(args[0]));
//////            FileOutputFormat.setOutputPath(job, new Path(args[1]));
//////            ********* basic input addition - end *************
////
////
////
////        // ************* multiple input addition ************
////        // parameters:
////        // 0 - DPMin
////        // 1 - numOfFiles - the number of files from the biarcs database that
////        //		we want to compute. 20 -> 20% (20 files). 100 -> 100% (100 files)
////
////        String paths = "";
////        String index;
////        int numOfFiles = Integer.parseInt(args[4]);
////        System.out.println("number of input files: " + numOfFiles);
////        System.out.println("the input paths:");
////        for (int i = 0; i < numOfFiles && i < 99; i++ ){
////            if (i < 10){
////                index = "0" + i;
////            } else {
////                index = "" + i;
////            }
////            System.out.println("adding " + "biarcs."+index+"-of-99 to the input paths");
////            paths += args[0] + "biarcs."+index+"-of-99,";
////        }
////
////        paths = paths.substring(0, paths.length()-1);
////        //comma separated paths to the list of inputs for the map-reduce job
////        FileInputFormat.addInputPaths(job, paths);
////        FileOutputFormat.setOutputPath(job, new Path(args[1]));
////        // ************* multiple input addition - end *********
////        MultipleOutputs.addNamedOutput(job, "biarcs", TextOutputFormat.class, NullWritable.class, Text.class);
////        MultipleOutputs.addNamedOutput(job, "features", TextOutputFormat.class, NullWritable.class, Text.class);
////
////        System.out.println("running");
////        boolean result = job.waitForCompletion(true);
//
//
//
//    /*Counters counters = job.getCounters();
//    System.out.println();
//    System.out.println("All Info: Job1");
//    for (CounterGroup group : counters) {
//        System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
//        System.out.println("  number of counters in this group: " + group.size());
//        for (Counter counter : group) {
//            System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
//        }
//    }*/
//
//        System.exit(result ? 0 : 1);
//    }
//}
//

/*********************************** previous version ************************************/

//package build_dataset_package;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
//import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
//
///**
// * Created by yonatanalexander on 7/10/14.
// */
//public class MainBuildDataset {
//
//	private static final int NUM_OF_REDUCERS = 1;
//
//	/******************* main for local runs **************************/
//
////	public static void main(String[] args) throws Exception{
////		Configuration conf = new Configuration();
////		conf.setInt("mapred.tasktracker.reduce.tasks.maximum", 1);
//////		conf.set("hypernymFile", args[2]);
//////		conf.set("vectorFile", args[3]);
//////		conf.set("vectorFile", "C:\\Netta\\University\\workspace\\DSProject\\features.txt");
//////		conf.set("DPMin", args[4]);
////		//Defining job parameters
////		Job job = new Job(conf, "BuildDataset");
//////		job.setInputFormatClass(SequenceFileInputFormat.class);
////        job.setInputFormatClass(TextInputFormat.class);
////		job.setJarByClass(DatasetCompute.class);
////		job.setMapperClass(DatasetCompute.DatasetComputeMapJob.class);
////		job.setMapOutputKeyClass(KeyWordCouple.class);
////		job.setMapOutputValueClass(Text.class);
//////		job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
////		job.setCombinerClass(DatasetCompute.DatasetComputeCombinerjob.class);
////		job.setReducerClass(DatasetCompute.DatasetComputeReduceJob.class);
////		job.setNumReduceTasks(NUM_OF_REDUCERS);
////		job.setOutputKeyClass(Text.class);
////		job.setOutputValueClass(Text.class);
//////		FileInputFormat.addInputPath(job, new Path(args[0]));
////		TextInputFormat.addInputPath(job, new Path(args[0]));
////		FileOutputFormat.setOutputPath(job, new Path(args[1]));
////		boolean result = job.waitForCompletion(true);
////		//System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());
////
////
////
////		/*Counters counters = job.getCounters();
////        System.out.println();
////        System.out.println("All Info: Job1");
////        for (CounterGroup group : counters) {
////            System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
////            System.out.println("  number of counters in this group: " + group.size());
////            for (Counter counter : group) {
////                System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
////            }
////        }*/
////
////		System.exit(result ? 0 : 1);
////	}
//
//	/******************* main for S3 runs **************************/
//
//	public static void main(String[] args) throws Exception{
//        Configuration conf = new Configuration();
//        conf.setInt("mapred.tasktracker.reduce.tasks.maximum", 1);
////        conf.set("vectorFile", args[2]);
////        System.out.println("vectorFile: " + args[2]);
////        conf.set("hypernymFile", args[3]);
////        conf.set("DPMin", args[4]);
//        //Defining job parameters
//        Job job = new Job(conf, "BuildDataset");
////        job.setInputFormatClass(SequenceFileInputFormat.class);
//        job.setInputFormatClass(TextInputFormat.class);
//        job.setJarByClass(DatasetCompute.class);
//        job.setMapperClass(DatasetCompute.DatasetComputeMapJob.class);
//        job.setMapOutputKeyClass(KeyWordCouple.class);
//        job.setMapOutputValueClass(Text.class);
////        job.setPartitionerClass(DatasetCompute.DatasetComputePartitioner.class);
//        job.setCombinerClass(DatasetCompute.DatasetComputeCombinerjob.class);
//        job.setReducerClass(DatasetCompute.DatasetComputeReduceJob.class);
//		job.setNumReduceTasks(NUM_OF_REDUCERS);
//        job.setOutputKeyClass(Text.class);
//        job.setOutputValueClass(Text.class);
////        FileInputFormat.addInputPath(job, new Path(args[0]));
//        TextInputFormat.addInputPath(job, new Path(args[0]));
//        FileOutputFormat.setOutputPath(job, new Path(args[1]));
//        boolean result = job.waitForCompletion(true);
//        //System.out.println("MAP_OUTPUT_RECORDS - Job1: "+job.getCounters().findCounter("org.apache.hadoop.mapred.Task$Counter","MAP_OUTPUT_RECORDS").getValue());
//
//
//
//    /*Counters counters = job.getCounters();
//    System.out.println();
//    System.out.println("All Info: Job1");
//    for (CounterGroup group : counters) {
//        System.out.println("* Counter Group: " + group.getDisplayName() + " (" + group.getName() + ")");
//        System.out.println("  number of counters in this group: " + group.size());
//        for (Counter counter : group) {
//            System.out.println("  - " + counter.getDisplayName() + ": " + counter.getName() + ": "+counter.getValue());
//        }
//    }*/
//
//        System.exit(result ? 0 : 1);
//    }
//}
