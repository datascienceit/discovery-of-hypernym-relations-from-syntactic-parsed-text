package aggregation;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.TreeMap;

public class AggregateVectors {

    private static TreeMap<String, Integer> featureTreeMap;
    private static ArrayList<String> newData;


    public static void main(String[] args) throws IOException {
        featureTreeMap = new TreeMap<String, Integer>();
        newData = new ArrayList<String>();
        //for s3
        AWSCredentials credentials = new PropertiesCredentials(
                AggregateVectors.class
                        .getResourceAsStream("AwsCredentials.properties"));
        AmazonS3 s3 = new AmazonS3Client(credentials);
        //TODO read all Vector files
//    	String root = "C:\\Netta\\University\\2nd_Degree\\Year_A\\Distributed_System_Programming\\Assignment3\\testRuns\\output";
        for (int i = 0; i < 1000; i++) {
            String ordinal = "" + i;
            if (ordinal.length() == 1) {
                ordinal = 0 + ordinal;
            }
            try {
//    			System.out.println("~~~~~~~~~~~~~~~~~~~~reading file part-r-000"+ordinal + " ~~~~~~~~~~~~~~~~~~~~~~~~~~`");

                //this is for s3
            	String fileName;
            	if (ordinal.length() == 2){
            		fileName = "part-r-000" + ordinal;
            	} else {
            		fileName = "part-r-00" + ordinal;
            	}
            	// TODO make sure we set the path correctly 
                S3Object s3obj = s3.getObject(new GetObjectRequest("dsp-assignment3-netta/output1-dpmin5-standard-nof100", fileName));
                S3ObjectInputStream content = s3obj.getObjectContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(content));

                //this is for local testing
//                BufferedReader br = null;
//                System.out.println("reading features files");
//                String input = args[0];
//                br = new BufferedReader(new FileReader(input));


                try {

                    for (String line; (line = br.readLine()) != null; ) {

//    					System.out.println(line);
                        String[] split = line.split("###");
                        featureTreeMap.put(split[0], 1);
                        String[] splitData = split[1].split("!!!");
                        if (splitData != null) {
                            for (int j = 0; j < splitData.length; j++) {
//                                System.out.println(splitData[j]);
                                if (splitData[j] != null && splitData[j] != "") {
                                    newData.add(splitData[j]);
                                }

                            }
                        }
                    }

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } catch (AmazonServiceException e) {
                e.getMessage();
//    			System.out.println(ordinal);
                break;
            } catch (AmazonClientException e) {
                e.getMessage();
//    			System.out.println(ordinal);
                break;
            }
//    		System.out.println("\n# of files read: " + i);
        }

//    	for (String s : featureTreeMap.keySet()){
//    		System.out.println(s);
//    	}

        System.out.println("writing paths to file");
//    	PrintWriter writer = new PrintWriter("features.txt", "UTF-8");
        PrintWriter treeMapWriter = new PrintWriter("features-dpmin5-stan-nof100.txt", "UTF-8");

        for (String str : featureTreeMap.keySet()) {
            treeMapWriter.println(str);
        }

//    	writer.close();
        treeMapWriter.close();

        PrintWriter writer = new PrintWriter("data-dpmin5-stan-nof100.txt", "UTF-8");

        for (String str : newData) {
            writer.println(str);
        }
        writer.close();

        // Uploading the paths list to S3
        System.out.println("Uploading the paths list to S3");
        String inputFilePath = System.getProperty("user.dir");
        inputFilePath += "\\features-dpmin5-stan-nof100.txt";

        File path = new File(inputFilePath);
        PutObjectRequest putObjectRequest = new PutObjectRequest("dsp-assignment3-netta/aggregate-vectors", "features-dpmin5-stan-nof100", path);
        // Sets the optional pre-configured access control policy to use
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        // Uploads the list to Amazon S3 bucket
        s3.putObject(putObjectRequest);

        inputFilePath = System.getProperty("user.dir");
        inputFilePath += "\\data-dpmin5-stan-nof100.txt";

        path = new File(inputFilePath);
        putObjectRequest = new PutObjectRequest("dsp-assignment3-netta/aggregate-vectors", "data-dpmin5-stan-nof100", path);
        // Sets the optional pre-configured access control policy to use
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        // Uploads the list to Amazon S3 bucket
        s3.putObject(putObjectRequest);

        System.out.println("the features list is on S3");
    }
}
