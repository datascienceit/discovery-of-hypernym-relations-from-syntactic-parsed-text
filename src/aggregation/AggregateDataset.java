package aggregation;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class AggregateDataset {
	private static TreeMap<String, Integer> featureTreeMap;
	private static ArrayList<String> observations;

	
	/******************************  main for local runs  *******************************/
	
//	public static void main(String[] args) throws IOException {
//		featureTreeMap = new TreeMap<String, Integer>();
//		observations = new ArrayList<String>();
//
//		System.out.println("reading features vector");
//
//		String line ="";
//		String dPath ="";
//
//
//		AWSCredentials credentials = new PropertiesCredentials(
//				AggregateVectors.class
//				.getResourceAsStream("AwsCredentials.properties"));
//		AmazonS3 s3 = new AmazonS3Client(credentials);
//
//		BufferedReader br = null;
//		String inputVector = args[1];
//		String inputData = args[0];
//		br = new BufferedReader(new FileReader(inputVector));
//
//		try {
//			while ((line = br.readLine()) != null) {
//				dPath = line.toString();
//				if (dPath != null && dPath != "") {
//					featureTreeMap.put(dPath, 0);
//				}
//			}
//		}catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		br.close();
//		System.out.println("#relevantPaths: " + featureTreeMap.size());
//
//		//write features for header TODO while might be for nothing
//		Iterator it = featureTreeMap.entrySet().iterator();
//		int i = 0;
//		while (it.hasNext()) {
//			Map.Entry pairs = (Map.Entry) it.next();
//			featureTreeMap.put(pairs.getKey().toString(), i);
//			i++;
//		}
//
//		//for printing the vector paths
//		//        it = featureTreeMap.entrySet().iterator();
//		//        while (it.hasNext()) {
//		//            Map.Entry pairs = (Map.Entry) it.next();
//		//            System.out.println(pairs.getKey() + " " + pairs.getValue());
//		//        }
//
//		//this this for local testing
//		br = new BufferedReader(new FileReader(inputData));
//
//		//reading feature vector for Header
//		try {
//
//			for (String observation; (observation = br.readLine()) != null; ) {
//				//                    System.out.println(observation);
//				observations.add(observation);
//			}
//
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		//    	for (String s : featureTreeMap.keySet()){
//		//    		System.out.println(s);
//		//    	}
//
//		System.out.println("writing paths to file");
//		PrintWriter writer = new PrintWriter("dataset2.txt", "UTF-8");
//		String header = "";
//		//write header
//		for (String str : featureTreeMap.keySet()) {
//			header = header + str +",";
//		}
//		header = header + "target";
//		writer.println(header);
//
//		//write observations
//		for (String str : observations) {
//			writer.println(str);
//		}
//		writer.close();
//		// Uploading the paths list to S3
//		System.out.println("Uploading the paths list to S3");
//		String inputFilePath = System.getProperty("user.dir");
//		inputFilePath += "\\dataset2.txt";
//
//		File path = new File(inputFilePath);
//		PutObjectRequest putObjectRequest = new PutObjectRequest("dsp-assignment3-netta", "features2", path);
//		// Sets the optional pre-configured access control policy to use
//		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
//		// Uploads the list to Amazon S3 bucket
//		s3.putObject(putObjectRequest);
//
//		//        inputFilePath = System.getProperty("user.dir");
//		//        inputFilePath += "\\features2.txt";
//		//
//		//        path = new File(inputFilePath);
//		//        putObjectRequest = new PutObjectRequest("dsp-assignment3-netta", "data2", path);
//		//        // Sets the optional pre-configured access control policy to use
//		//        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
//		//        // Uploads the list to Amazon S3 bucket
//		//        s3.putObject(putObjectRequest);
//
//		System.out.println("the features list is on S3");
//
//
//	}



/******************************  main for S3 runs  *******************************/


	public static void main(String[] args) throws IOException {
		featureTreeMap = new TreeMap<String, Integer>();
		observations = new ArrayList<String>();

		System.out.println("reading features vector");

		String line ="";
		String dPath ="";


		AWSCredentials credentials = new PropertiesCredentials(
				AggregateVectors.class
				.getResourceAsStream("AwsCredentials.properties"));
		AmazonS3 s3 = new AmazonS3Client(credentials);

//		S3Object s3obj = s3.getObject(new GetObjectRequest("dsp-assignment3-netta", "feature_vector.txt"));
		S3Object s3obj = s3.getObject(new GetObjectRequest("dsp-assignment3-netta/aggregate-vectors", "features-dpmin5-stan-nof100"));
		S3ObjectInputStream content = s3obj.getObjectContent();
		BufferedReader br = new BufferedReader(new InputStreamReader(content));

		try {
			while ((line = br.readLine()) != null) {
				dPath = line.toString();
				if (dPath != null && dPath != "") {
					featureTreeMap.put(dPath, 0);
				}
			}
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		br.close();
		System.out.println("#relevantPaths: " + featureTreeMap.size());

		//write features for header TODO while might be for nothing
		Iterator it = featureTreeMap.entrySet().iterator();
		int i = 0;
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry) it.next();
			featureTreeMap.put(pairs.getKey().toString(), i);
			i++;
		}

		//for printing the vector paths
//		it = featureTreeMap.entrySet().iterator();
//		        while (it.hasNext()) {
//		            Map.Entry pairs = (Map.Entry) it.next();
//		            System.out.println(pairs.getKey() + " " + pairs.getValue());
//		        }

		for (i = 0; i < 1000; i++) {
			String ordinal = "" + i;
			if (ordinal.length() == 1) {
				ordinal = 0 + ordinal;
			}
			try {
				//this is for s3
				String fileName;
            	if (ordinal.length() == 2){
            		fileName = "part-r-000" + ordinal;
            	} else {
            		fileName = "part-r-00" + ordinal;
            	}
				s3obj = s3.getObject(new GetObjectRequest("dsp-assignment3-netta/output2-dpmin5-stan-nof100", fileName));
				content = s3obj.getObjectContent();
				System.out.println("reading file " + fileName);
				br = new BufferedReader(new InputStreamReader(content));

				//reading feature vector for Header
				try {
					for (String observation; (observation = br.readLine()) != null; ) {
//                        System.out.println(observation);
						observations.add(observation);
					}
					System.out.println("finished reading file " + fileName);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (AmazonServiceException e) {
				e.getMessage();
				//    			System.out.println(ordinal);
				break;
			} catch (AmazonClientException e) {
				e.getMessage();
				//    			System.out.println(ordinal);
				break;
			}
			//    		System.out.println("\n# of files read: " + i);
		}

		//    	for (String s : featureTreeMap.keySet()){
		//    		System.out.println(s);
		//    	}

		System.out.println("writing paths to file");
		PrintWriter writer = new PrintWriter("dataset2-dpmin5-stan-nof100.txt", "UTF-8");
		String header = "";
		//write header
		for (String str : featureTreeMap.keySet()) {
			header = header + str +",";
		}
		header = header + "target";
		writer.println(header);

		//write observations
		for (String str : observations) {
			writer.println(str);
		}
		writer.close();
		
		
//		// Uploading the paths list to S3
//		System.out.println("Uploading the paths list to S3");
//		String inputFilePath = System.getProperty("user.dir");
//		inputFilePath += "\\dataset2-dpmin5-stan-nof20.txt";
//
//		File path = new File(inputFilePath);
//		PutObjectRequest putObjectRequest = new PutObjectRequest("dsp-assignment3-netta/aggregate-dataset", "dataset-dpmin5-stan-nof20", path);
//		// Sets the optional pre-configured access control policy to use
//		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
//		// Uploads the list to Amazon S3 bucket
//		s3.putObject(putObjectRequest);



		System.out.println("the features list is on S3");
	}
}

//inputFilePath = System.getProperty("user.dir");
//inputFilePath += "\\features2.txt";
//
//path = new File(inputFilePath);
//putObjectRequest = new PutObjectRequest("dsp-assignment3-netta", "data2", path);
//// Sets the optional pre-configured access control policy to use
//putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
//// Uploads the list to Amazon S3 bucket
//s3.putObject(putObjectRequest);