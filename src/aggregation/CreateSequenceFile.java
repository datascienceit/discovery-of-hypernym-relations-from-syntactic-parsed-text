package aggregation;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.net.URI;

/**
 * Created by yonatanalexander on 7/10/14.
 */
public class CreateSequenceFile {
    private static final String[] DATA = { "One, two, buckle my shoe", "Three, four, shut the door", "Five, six, pick up sticks", "Seven, eight, lay them straight", "Nine, ten, a big fat hen" };
//    public static void main(String[] args) throws IOException,
//            InterruptedException, ClassNotFoundException {
//
//        Configuration conf = new Configuration();
//        Job job = new Job(conf);
//        job.setJobName("Convert Text");
//        job.setJarByClass(Mapper.class);
//
//        job.setMapperClass(Mapper.class);
//        job.setReducerClass(Reducer.class);
//
//        // increase if you need sorting or a special number of files
//        job.setNumReduceTasks(0);
//
//        job.setOutputKeyClass(LongWritable.class);
//        job.setOutputValueClass(Text.class);
//
//        job.setOutputFormatClass(SequenceFileOutputFormat.class);
//        job.setInputFormatClass(TextInputFormat.class);
//
//        TextInputFormat.addInputPath(job, new Path("/Users/yonatanalexander/Desktop/DSProject/all_inputs/biarcs.71-of-99"));
//        SequenceFileOutputFormat.setOutputPath(job, new Path("/Users/yonatanalexander/Desktop/DSProject/all_inputs/inputbig"));
//
//        // submit and wait for completion
//        job.waitForCompletion(true);
//    }

    public static void main( String[] args) throws IOException {
        String uri = args[ 0];
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(uri), conf);
        Path path = new Path( uri);
        IntWritable key = new IntWritable();
        Text value = new Text();
        SequenceFile.Writer writer = null;
        try {
            writer = SequenceFile.createWriter( fs, conf, path, key.getClass(), value.getClass());
            for (int i = 0; i < 100; i ++) {
                key.set( 100 - i);
                value.set( DATA[ i % DATA.length]);
                System.out.printf("[% s]\t% s\t% s\n", writer.getLength(), key, value);
                writer.append( key, value); }
        } finally
        { IOUtils.closeStream(writer);
        }
    }
}

