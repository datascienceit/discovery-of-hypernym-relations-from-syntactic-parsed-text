package main_dsproject;

import java.io.IOException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.PlacementType;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;

public class MainDSProject {
	public static void main(String[] args) throws Exception	{
		if(args.length == 0)		{
			System.out.println("Usage: java -jar DSProject.jar MainDSProject <DPMin> <inver> <numberOfFiles>");
			System.exit(1);
		}

		String DPMin = args[0];
		try{
			int k = Integer.parseInt(DPMin);
		} catch (NumberFormatException e){
			System.out.println("DPMin parameter is not a number, assigning a default value - 5");
			DPMin = "5";
		}
		System.out.println("DPMin value is " + DPMin);
		
		String inverse = args[1];
		System.out.println("inverse value is " + inverse);
		
		String numOfInputFiles = "20";
		if (args.length > 2){
			numOfInputFiles = args[2];
		}
		try{
			int k = Integer.parseInt(numOfInputFiles);
		} catch (NumberFormatException e){
			System.out.println("numOfInputFiles parameter is not a number, assigning a default value - 20");
			numOfInputFiles = "20";
		}
		System.out.println("numOfInputFiles value is " + numOfInputFiles);

		//corpus = "s3://bgudsp142/syntactic-ngram/biarcs/biarcs.xx-of-99";
		String corpus = "s3://bgudsp142/syntactic-ngram/biarcs/";

		AWSCredentials credentials = null;
		try{
			credentials = new PropertiesCredentials(MainDSProject.class.getResourceAsStream("AwsCredentials.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		AmazonElasticMapReduce mapReduce = new AmazonElasticMapReduceClient(credentials);

		//************************************************ job0-debug ************************************************//

		HadoopJarStepConfig hadoopJarStepDebug0 = new HadoopJarStepConfig()
		.withJar("s3://us-east-1.elasticmapreduce/libs/script-runner/script-runner.jar") // This should be a full map reduce application.
		.withArgs("s3://us-east-1.elasticmapreduce/libs/state-pusher/0.1/fetch");

		StepConfig stepConfigDebug = new StepConfig()
		.withName("debug")
		.withHadoopJarStep(hadoopJarStepDebug0)
		.withActionOnFailure("TERMINATE_JOB_FLOW");

		//*************************************************** job1 ***************************************************//		

		HadoopJarStepConfig hadoopJarStep1 = new HadoopJarStepConfig()
		.withJar("s3n://netta-dsp-ass3/FeatureVector.jar") // This should be a full map reduce application.
		.withMainClass("feature_vector_package.MainBuildFeatureVector")
		.withArgs(corpus,
				"s3n://netta-dsp-ass3/output1-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles+"/",
				DPMin,
				inverse,
				numOfInputFiles);

		StepConfig stepConfig1 = new StepConfig()
		.withName("ComputeFeatureVector-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles)
		.withHadoopJarStep(hadoopJarStep1)
		.withActionOnFailure("TERMINATE_JOB_FLOW");

		//*************************************************** job2 ***************************************************// 		

		HadoopJarStepConfig hadoopJarStep2 = new HadoopJarStepConfig()
		.withJar("s3n://netta-dsp-ass3/Dataset.jar") // This should be a full map reduce application.
		.withMainClass("build_dataset_package.MainBuildDataset")
		.withArgs("s3n://netta-dsp-ass3/output1-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles+"/",
				"s3n://netta-dsp-ass3/output2-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles+"/",
				inverse);

		StepConfig stepConfig2 = new StepConfig()
		.withName("ComputeDataset-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles)
		.withHadoopJarStep(hadoopJarStep2)
		.withActionOnFailure("TERMINATE_JOB_FLOW");
		
		//*************************************************** job3 ***************************************************// 		

//		HadoopJarStepConfig hadoopJarStep3 = new HadoopJarStepConfig()
//		.withJar("s3n://dsp-assignment2-netta/partA.jar") // This should be a full map reduce application.
//		.withMainClass("job3package.MainJob3")
//		.withArgs("s3n://dsp-assignment2-netta/out2/", "s3n://dsp-assignment2-netta/out3/",K);// Add a K parameter
//
//		StepConfig stepConfig3 = new StepConfig()
//		.withName("job3")
//		.withHadoopJarStep(hadoopJarStep3)
//		.withActionOnFailure("TERMINATE_JOB_FLOW");

		//--------------------------------------------------------------------------------------------------------------//

		JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
		.withInstanceCount(10)
		.withMasterInstanceType(InstanceType.M1Medium.toString())
		.withSlaveInstanceType(InstanceType.M1Medium.toString())
//		.withMasterInstanceType(InstanceType.M1Small.toString())
//		.withSlaveInstanceType(InstanceType.M1Small.toString())
		.withHadoopVersion("2.4.0")
//		.withHadoopVersion("0.20")
//		.withHadoopVersion("1.0")
		.withEc2KeyName("netta_key_pair_01")
		.withKeepJobFlowAliveWhenNoSteps(false)
		.withPlacement(new PlacementType("us-east-1a"));

		RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
		.withName("dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles)
		.withInstances(instances)
		.withSteps(stepConfigDebug,stepConfig1,stepConfig2)
		.withLogUri("s3n://dsp-assignment3-netta/logs/")
		.withAmiVersion("3.1.0");
//		.withAmiVersion("2.4.6");

		RunJobFlowResult runJobFlowResult = mapReduce.runJobFlow(runFlowRequest);
		String jobFlowId = runJobFlowResult.getJobFlowId();

		System.out.println("Ran job flow with id: " + jobFlowId);
	}
}




//package main_dsproject;
//
//import java.io.IOException;
//
//import com.amazonaws.auth.AWSCredentials;
//import com.amazonaws.auth.PropertiesCredentials;
//import com.amazonaws.services.ec2.model.InstanceType;
//import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
//import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
//import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
//import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
//import com.amazonaws.services.elasticmapreduce.model.PlacementType;
//import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
//import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
//import com.amazonaws.services.elasticmapreduce.model.StepConfig;
//
//public class MainDSProject {
//	public static void main(String[] args) throws Exception	{
//		if(args.length == 0)		{
//			System.out.println("Usage: java -jar DSProject.jar MainDSProject <DPMin> <inver> <numberOfFiles>");
//			System.exit(1);
//		}
//
//		String DPMin = args[0];
//		try{
//			int k = Integer.parseInt(DPMin);
//		} catch (NumberFormatException e){
//			System.out.println("DPMin parameter is not a number, assigning a default value - 5");
//			DPMin = "5";
//		}
//		
//		String inverse = args[1];
//		
//		String numOfInputFiles = "20";
//		if (args.length > 2){
//			numOfInputFiles = args[2];
//		}
//		try{
//			int k = Integer.parseInt(numOfInputFiles);
//		} catch (NumberFormatException e){
//			System.out.println("numOfInputFiles parameter is not a number, assigning a default value - 20");
//			numOfInputFiles = "20";
//		}
//
//		//corpus = "s3://bgudsp142/syntactic-ngram/biarcs/biarcs.xx-of-99";
//		String corpus = "s3://bgudsp142/syntactic-ngram/biarcs/";
//
//		AWSCredentials credentials = null;
//		try{
//			credentials = new PropertiesCredentials(MainDSProject.class.getResourceAsStream("AwsCredentials.properties"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		AmazonElasticMapReduce mapReduce = new AmazonElasticMapReduceClient(credentials);
//
//		//************************************************ job0-debug ************************************************//
//
//		HadoopJarStepConfig hadoopJarStepDebug0 = new HadoopJarStepConfig()
//		.withJar("s3://us-east-1.elasticmapreduce/libs/script-runner/script-runner.jar") // This should be a full map reduce application.
//		.withArgs("s3://us-east-1.elasticmapreduce/libs/state-pusher/0.1/fetch");
//
//		StepConfig stepConfigDebug = new StepConfig()
//		.withName("debug")
//		.withHadoopJarStep(hadoopJarStepDebug0)
//		.withActionOnFailure("TERMINATE_JOB_FLOW");
//
//		//*************************************************** job1 ***************************************************//		
//
//		HadoopJarStepConfig hadoopJarStep1 = new HadoopJarStepConfig()
//		.withJar("s3n://dsp-assignment3-netta/FeatureVector.jar") // This should be a full map reduce application.
//		.withMainClass("feature_vector_package.MainBuildFeatureVector")
//		.withArgs(corpus, "s3n://dsp-assignment3-netta/output1-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles+"/", DPMin, inverse, numOfInputFiles);
//
//		StepConfig stepConfig1 = new StepConfig()
//		.withName("ComputeFeatureVector")
//		.withHadoopJarStep(hadoopJarStep1)
//		.withActionOnFailure("TERMINATE_JOB_FLOW");
//
//		//*************************************************** job2 ***************************************************// 		
//
//		HadoopJarStepConfig hadoopJarStep2 = new HadoopJarStepConfig()
//		.withJar("s3n://dsp-assignment3-netta/Dataset.jar") // This should be a full map reduce application.
//		.withMainClass("build_dataset_package.MainBuildDataset")
//		.withArgs("s3n://dsp-assignment3-netta/output1-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles+"/",
//				"s3n://dsp-assignment3-netta/output2-dpmin"+DPMin+"-"+inverse+"-"+"nof"+numOfInputFiles+"/");
//
//		StepConfig stepConfig2 = new StepConfig()
//		.withName("ComputeDataset")
//		.withHadoopJarStep(hadoopJarStep2)
//		.withActionOnFailure("TERMINATE_JOB_FLOW");
//		
//		//*************************************************** job3 ***************************************************// 		
//
////		HadoopJarStepConfig hadoopJarStep3 = new HadoopJarStepConfig()
////		.withJar("s3n://dsp-assignment2-netta/partA.jar") // This should be a full map reduce application.
////		.withMainClass("job3package.MainJob3")
////		.withArgs("s3n://dsp-assignment2-netta/out2/", "s3n://dsp-assignment2-netta/out3/",K);// Add a K parameter
////
////		StepConfig stepConfig3 = new StepConfig()
////		.withName("job3")
////		.withHadoopJarStep(hadoopJarStep3)
////		.withActionOnFailure("TERMINATE_JOB_FLOW");
//
//		//--------------------------------------------------------------------------------------------------------------//
//
//		JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
//		.withInstanceCount(2)
//		.withMasterInstanceType(InstanceType.M1Medium.toString())
//		.withSlaveInstanceType(InstanceType.M1Medium.toString())
////		.withMasterInstanceType(InstanceType.M1Small.toString())
////		.withSlaveInstanceType(InstanceType.M1Small.toString())
//		.withHadoopVersion("2.4.0")
////		.withHadoopVersion("0.20")
////		.withHadoopVersion("1.0")
//		.withEc2KeyName("netta_key_pair_01")
//		.withKeepJobFlowAliveWhenNoSteps(false)
//		.withPlacement(new PlacementType("us-east-1a"));
//
//		RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
//		.withName("BuildFeatureVector")
//		.withInstances(instances)
//		.withSteps(stepConfigDebug,stepConfig1,stepConfig2)
//		.withLogUri("s3n://dsp-assignment3-netta/logs/")
//		.withAmiVersion("3.1.0");
////		.withAmiVersion("2.4.6");
//
//		RunJobFlowResult runJobFlowResult = mapReduce.runJobFlow(runFlowRequest);
//		String jobFlowId = runJobFlowResult.getJobFlowId();
//
//		System.out.println("Ran job flow with id: " + jobFlowId);
//	}
//}